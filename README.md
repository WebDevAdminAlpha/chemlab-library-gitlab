# GitLab Library for Chemlab

### Usage

Add the chemlab-library-gitlab gem

```ruby
gem 'chemlab-library-gitlab'
```

Mount the library to the Chemlab configuration

```ruby
require 'gitlab'

Chemlab.configure do |config|
  config.libraries = [Gitlab]
end
```
