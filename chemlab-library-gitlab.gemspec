# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = 'chemlab-library-gitlab'
  spec.version = '0.1.0'
  spec.authors = ['GitLab Quality']
  spec.email = ['quality@gitlab.com']

  spec.summary = 'Page Libraries for GitLab.com'
  spec.homepage = 'https://about.gitlab.com'
  spec.license = 'MIT'

  spec.files = `git ls-files -- lib/*`.split("\n")

  spec.require_paths = ['lib']

  # Some dependencies are pinned, to prevent new cops from breaking the CI pipelines
  spec.add_runtime_dependency 'chemlab', '~> 0.1'

  spec.add_development_dependency 'rspec', '~> 3.7'
end
