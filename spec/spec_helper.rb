# frozen_string_literal: true

require 'chemlab'
require_relative '../lib/gitlab'

Chemlab.configure do |config|
  config.base_url = 'https://gitlab.com'

  config.browser = :chrome, { headless: true, options: { args: %w[--no-sandbox --disable-gpu --disable-dev-shm-usage
                         --window-size=1480,2200 --ignore-certificate-errors] } }
end
