# frozen_string_literal: true

module Gitlab
  RSpec.describe 'Gitlab' do
    describe 'Login' do
      before do
        Page::Login.perform(&:visit)
      end

      it 'shows the login page' do
        Page::Login.perform do |login|
          expect(login.login_field_element).to exist
          expect(login.password_field_element).to exist
          expect(login.sign_in_button_element).to exist
        end
      end
    end
  end
end
